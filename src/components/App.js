import React, { Component } from 'react';
import Header from "./Header"
import Todos from "./Todos"
import TodoDetail from "./TodoDetail"
import {BrowserRouter as Router, Switch, Route, Redirect} from "react-router-dom"
import Info from './Info';

const NoMatch = ({location}) => (
    <div>
        <h3>Error<code>{location.pathname}</code></h3>
    </div>
)

function  App() {
    return (
        <Router>
            <div className="App">
                  <Header />
                  <Switch>
                      <Route path="/" exact component={Info}/>
                      <Route path="/todo" exact component={Todos}/>
                      <Route path="/todo/:id" component={TodoDetail} />
                  </Switch>
            </div>
        </Router>
    );
}

export default App;

import React from "react"
import Nav from "./Nav"

function Info() {
    return(
        <div className="nav" className="info" >
            <Nav />
            <h3>Used Librarys:</h3>
            <ul>
                <li><a href="https://github.com/facebook/react">React</a></li>
                <li><a href="https://github.com/ReactTraining/react-router">React Router</a></li>
            </ul>
            <br />
            <h3>For Develpoment:</h3>
            <ul>
                <li><a href="https://github.com/typicode/json-server">JSON Server</a></li>
                <li><a href="https://github.com/facebook/create-react-app">Create React App</a></li>
            </ul>
        </div>
    )
}

export default Info
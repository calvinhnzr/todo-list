import React from "react"

function Header() {
    return(
        <header>
            <h2>Hacking Talents</h2>
            <h1>Todo List</h1>
        </header>
    )
}

export default Header
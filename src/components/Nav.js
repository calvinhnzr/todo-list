import React from "react"
import {Link} from "react-router-dom"

function Nav() {
    return(
        <div className="nav">
            <h3>Path:</h3>
            <ul className="path-list">
                <Link to="/">
                    <li>/</li>
                </Link>
                <Link to="/todo">
                    <li>/todo</li>
                </Link>
            </ul>
        </div>
    )
}

export default Nav
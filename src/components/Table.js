import React, { Component } from "react"
import {Link} from "react-router-dom"

class TableBody extends Component { 
    constructor(props) {
        super(props)
    }

    render() {
        const rows = this.props.data.map((item) => {
            return (
                <tr key={item.id} >                        
                <td className="done">{item.done ? <div className="box-checked" ><div></div></div> : <div className="box-unchecked" ></div>}</td>
                <td 
                    className="description"
                    style={{textDecoration: item.done ? "line-through" : "none"}}
                >
                    {item.description}
                </td>
                <td className="edit">
                    <Link to={`/todo/${item.id}`}>
                        <span style={{color: "white"}}>Edit</span>
                    </Link>
                </td>
            </tr>
            )    
        })

        return <tbody>{rows}</tbody>
    }  
}  

const Table = (props) => {
    const { data } = props
    return (
        <table className="table">
            <TableBody data={data}/>
        </table>
    );
}

export default Table
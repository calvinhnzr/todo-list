import React, { Component} from "react"

class Form extends Component {
    constructor(props) {
        super(props)
        this.initialState = {
            id: "",
            description: "",
            done: false,
        }
        
        this.state = this.initialState
        this.handleChange = this.handleChange.bind(this)
        this.onFormSubmit = this.onFormSubmit.bind(this)
    }

    handleChange = event => {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    }

    onFormSubmit = (event) => {
        event.preventDefault();
        this.props.handleSubmit(this.state)
        this.setState(this.initialState);
        
    }
    
    render() {
        const { description } = this.state
        return(
            <form onSubmit={this.onFormSubmit} >
                <label for="description" className="form-label">Add a new item</label>
                <br />
                <input 
                    type="text"
                    name="description"
                    id="description"
                    value={description}
                    onChange={this.handleChange}
                    required
                />
                <button type="submit">
                    Submit
                </button>
            </form>
        )
    }
}

export default Form
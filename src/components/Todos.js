import React, {useState, useEffect} from "react"
import Nav from "./Nav"
import Form from "./Form"
import Table from "./Table"
// import {Link} from "react-router-dom"

function Todos() {

    useEffect(() => {
        fetchItems();
        },[])

    const [items, setItems] = useState([])

    // GET
    const fetchItems = async () => {
        const url = "http://localhost:8080/todo/"
        const data = await fetch(url)
        const items = await data.json()
        setItems(items)
    }

    // POST
    const handleSubmit = data => {
        const url = "http://localhost:8080/todo/"
        fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
        })
        .then(response => response.json())
        .catch(error => console.error("Error:", error))
        .then(()=> fetchItems())

    }

 

    return(
        <main>
            <Nav />
            <Form handleSubmit={handleSubmit}/>
            <Table data={items} />
        </main>
    )
}


export default Todos
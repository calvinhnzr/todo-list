import React, {useState, useEffect} from "react"
import Nav from "./Nav"

function TodoDetail(match) {
    useEffect(() => {
      fetchItem()
    }, []);

    const [item, setItem] = useState({})

    // GET
    const fetchItem = async () => {
        const url = `http://localhost:8080/todo/${match.match.params.id}`
        const data = await fetch(url)
        const item = await data.json()
        setItem(item)
         // working on 404 after /todo/
        if(httpStatus === 404) {
            console.log(true)
        } else {
            console.log(false)
        }
        
    }

    // DELETE
    const deleteItem = () => {
        fetch(`http://localhost:8080/todo/${match.match.params.id}`, {
            method: "DELETE",  
        })
        // display: none; if component may get "undo" feature
        .then(() => document.getElementById("hideEL").style.display = "none")
        .then( () => fetchItem())
    }

    // PATCH
    const patchItem = (data) => {
        const url = `http://localhost:8080/todo/${match.match.params.id}`
        fetch(url)
        .then(response => response.json())
        //flip "done" to true/false
        .then(
            data => fetch(url, {
                method: "PATCH",
                body: JSON.stringify({
                    done: !data.done
                }),
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                }
            })
            .then(() =>  fetchItem())
        )
    }

    return(       
        <main>
            <Nav />
            <form>
                <table className="detail-table table" id="hideEL">    
                    <tr key={item.id} >     
                        {/* Done */}
                        <td className="done">
                            <input 
                                type="checkbox"
                                name="done"
                                id="done"
                                onChange={patchItem}
                            />
                            <label for="done" className="done-label">
                                {item.done ? <div className="box-checked" ><div></div></div> : <div className="box-unchecked" ></div>}
                            </label> 
                        </td>
                        {/* Description */}
                        <td
                            className="description"
                            style={{textDecoration: item.done ? "line-through" : "none"}}
                        >
                            {item.description}
                        </td>
                        {/* Delete */}
                        <td className="delete">
                            <input
                                type="button"
                                value="Delete"
                                onClick={deleteItem}
                            />
                        </td>
                    </tr>
                </table>
            </form>   
        </main>
    )
}


export default TodoDetail
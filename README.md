# Todo List

A simple Todo App using the React Library.

## Getting Started

### Prerequisites

1. To run the App locally I recommend installing JSON-Server.
https://github.com/typicode/json-server

2. In addition use create-react-app to install react development environment.
https://github.com/facebook/create-react-app

3. Then install React Router in your react-app.
https://reacttraining.com/react-router/web/guides/quick-start

4. Clone this Repository and replace all files except the `node_modules` folder.

5. Hit `npm start`.

### Installing

#### 1. Install JSON Server

```
npm install -g json-server
```

Create a `db.json` file with some data

```json
{
  "todo": [
    {
      "id": 1,
      "description": "Hausaufgabe machen",
      "done": false
    }
  ]
}
```

Start JSON Server

```
json-server --watch db.json --port 8080
```

#### 2. Create React App Development Environment

```
npx create-react-app todo-list
```

#### 3. Install react router

Move to `todo-list`

```
cd todo-list
```

Install Library via npm

```
npm install react-router-dom
```

#### 4. Clone Repository

```
https://gitlab.com/calvinhnzr/todo-list.git
```

Replace all files from `todo-list` with all files from cloned Repository except `node_modules`

#### 5. Start React Development Environment

In `todo-list`:

```
npm start
```

## Questions

### Welche Stelle würdest Du als größte Hürde beschreiben?

Für mich war die größte Hürde diverse Bugs, die während der Entwicklungsphase entstanden sind, zu beheben.
Da ich bislang nur Erfahrung mit der `GET` Methode im Umgang von Api's hatte, entstanden dort die größte Hürden.

### Was gefällt Dir an der Lösung am besten?

Durch die Aufgabenstellung habe ich einige Technologien verwenden können,
die für die Lösung erforderlich und mir bis dahin noch unbekannt waren.
So habe ich mir einige Grundlagen der React Library "React Router" angeeignet.

### Was könnte man verbessern?

Die Aufgabenstellung sieht vor, dass man die Http Methode `DELETE` verwendet.
Stattdessen könnte man die Datensätze jediglich als "gelöscht" makieren um so ein Archiv aufbauen.
So werden die Datensätze nicht entgültig gelöscht und man könnte beispielsweise eine "Undo" Funktion in die Todo-List einbauen.

## Used Technologies

### Libraries:

React: https://reactjs.org/

React Router: https://reacttraining.com/react-router/web/guides/philosophy

### For Development:

JSON Server: https://github.com/typicode/json-server

Create React App: https://github.com/facebook/create-react-app

